class Document():
  def __init__(self, maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBanTheoNgay, giaBan) -> None:
    super().__init__()
    self.maTaiLieu = maTaiLieu
    self.nhaXuatBan = nhaXuatBan
    self.soLuongTonKho = soLuongTonKho
    self.soLuongBanTheoNgay = soLuongBanTheoNgay
    self.giaBan = giaBan
  
  def setmaTaiLieu(self, maTaiLieu):
    self.maTaiLieu = maTaiLieu

  def gemaTaiLieu(self):
    return self.maTaiLieu

  def setnhaXuatBan(self, nhaXuatBan):
    self.nhaXuatBan = nhaXuatBan

  def getnhaXuatBan(self):
    return self.nhaXuatBan

  def setsoLuongTonKho(self, soLuongTonKho):
    self.soLuongTonKho = soLuongTonKho

  def getsoLuongTonKho(self):
    return self.soLuongTonKho

  def setsoLuongBanTheoNgay(self, soLuongBanTheoNgay):
    self.soLuongBanTheoNgay = soLuongBanTheoNgay

  def getsoLuongBanTheoNgay(self):
    return self.soLuongBanTheoNgay

    
  def addSoLuongBanTheoNgay(self, SoLuongBanTheoNgay):
    self.soLuongBanTheoNgay.append(SoLuongBanTheoNgay)

  def setgiaBan(self, giaBan):
    self.giaBan = giaBan

  def getgiaBan(self):
    return self.giaBan

class SoLuongBanTheoNgay:
  def __init__(self, soLuongBan, ngay):
    self.SoLuongBan = soLuongBan
    self.ngay = ngay

  def setsoLuongBan(self, soLuongBan):
    self.soLuongBan = soLuongBan

  def getsoLuongBan(self):
    return self.soLuongBan

  def setngay(self, ngay):
    self.ngay = ngay

  def getngay(self):
    return self.ngay

class Sach(Document):
  def __init__(self, maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan, tacGia, soTrang) -> None:
    super().__init__(maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan)
    self.tacGia = tacGia
    self.soTrang = soTrang

  def settenTacGia(self, tenTacGia):
    self.tenTacGia = tenTacGia

  def gettenTacGia(self):
    return self.tenTacGia

  def setsoTrang(self, soTrang):
    self.soTrang = soTrang

  def getsoTrang(self):
    return self.soTrang

class TapChi(Document):
  def __init__(self, maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan, soPhatHanh, thangPhatHanh) -> None:
    super().__init__(maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan)
    self.soPhatHanh = soPhatHanh
    self.thangPhatHanh = thangPhatHanh
  
  def setsoPhatHanh(self, soPhatHanh):
    self.soPhatHanh = soPhatHanh

    def getsoPhatHanh(self):
      return self.soPhatHanh

    def setthangPhatHanh(self, thangPhatHanh):
      self.thangPhatHanh = thangPhatHanh

    def getthangPhatHanh(self):
      return self.thangPhatHanh

class Bao(Document):
  def __init__(self, maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan, ngayPhatHanh) -> None:
    super().__init__(maTaiLieu, nhaXuatBan, soLuongTonKho, soLuongBan, giaBan)
    self.ngayPhatHanh = ngayPhatHanh
  
  def setngayPhatHanh(self, ngayPhatHanh):
    self.ngayPhatHanh = ngayPhatHanh

  def getngayPhatHanh(self):
    return self.ngayPhatHanh