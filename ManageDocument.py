from typing import Self
from Document import *

class ManageDocument():
  def __init__(self, list) -> None:
    super().__init__()
    self.list = list
  
  def addDocument(self,doc: Document) -> None:
    checkExists = 0
    for i in list:
        if i.getmataiLieu().find(doc.getMaTaiLieu()) != -1:
            print("Đã tồn tại tài liệu có mã ", doc.getMaTaiLieu(), " .Vui lòng chọn mã khác")
            break
        checkExists = checkExists + 1
    if checkExists == len(list):
        list.append(doc)

  def findDocument(self,doc: Document) -> None:
    checkExists = 1
    for i in list:
        if i.getmaTaiLieu() == doc:
            print("Mã tài liệu:", i.getmaTaiLieu())
            print("Nhà xuất bản:", i.getnhaXuatBan())
            checkExists = 0
    if checkExists == 1:
        print("Không tìm thấy tài lệu có mã")

  def showDocuments(self) -> list:
    result_list = []
    for i in list:
        document_info = {
            "MaTaiLieu": i.getmaTaiLieu(),
            "NhaXuatBan": i.getnhaXuatBan(),
            "SoLuongTonKho": i.getSoLuongTonKho(),
            #đổ ra 1 mảng 
            "SoLuongBanTheoNgay": i.getsoLuongBanTheoNgay(),
            "GiaBan": i.getGiaBan()
        }

        if isinstance(i, Sach):
            document_info["TenTacGia"] = i.gettenTacGia()
            document_info["SoTrang"] = i.getsoTrang()

        if isinstance(i, TapChi):
            document_info["SoPhatHanh"] = i.getsoPhatHanh()
            document_info["ThangPhatHanh"] = i.getthangPhatHanh()

        if isinstance(i, Bao):
            document_info["NgayPhatHanh"] = i.getngayPhatHanh()

        result_list.append(document_info)

    return result_list

  def sortDocumentByQty(isAsc: bool) -> list:
    # truyền vào True thì sắp xếp bé đến lớn
    sorted_list = sorted(list, key=lambda x: x.getsoLuongTonKho(), reverse= isAsc)

    result_list = []
    for i in sorted_list:
        document_info = {
            "MaTaiLieu": i.getmaTaiLieu(),
            "NhaXuatBan": i.getnhaXuatBan(),
            "SoLuongTonKho": i.getsoLuongTonKho(),
            "SoLuongBanTheoNgay": i.getsoLuongBanTheoNgay(),
            "GiaBan": i.getgiaBan()
        }

        if isinstance(i, Sach):
            document_info["TenTacGia"] = i.gettenTacGia()
            document_info["SoTrang"] = i.getsoTrang()

        if isinstance(i, TapChi):
            document_info["SoPhatHanh"] = i.getsoPhatHanh()
            document_info["ThangPhatHanh"] = i.getthangPhatHanh()

        if isinstance(i, Bao):
            document_info["NgayPhatHanh"] = i.getngayPhatHanh()

        result_list.append(document_info)

    return result_list
    

  def showDocumentsByReleaseMonth(self, month: int) -> list:
    same_month_list = []
    month = int(month)
    for i in range(len(self.list)):
        if(hasattr(self.list[i], "getthangPhatHanh")) == True:
            if self.list[i].getthangPhatHanh() == month:
                same_month_list.append(self.list[i])
        return same_month_list

  def getDailyRevenue(self) -> list:
    list_document__daily_revenue = []
    for i in list:
        document__daily_revenue= {
            "MaTaiLieu": i.getmaTaiLieu(),
        }
            
        giaBan = i.getgiaBan()
        for j in i.getsoLuongBanTheoNgay():
          ngay = j.getngay()
          soLuongBan = j.getSoLuongBan()
          doanhThu = soLuongBan * giaBan
          document__daily_revenue["Ngay"] = ngay
          document__daily_revenue["DoanhThu"] = doanhThu
          list_document__daily_revenue.append(document__daily_revenue)
    return list_document__daily_revenue
         
  def sortDocumentByDailyRevenuef(self,isAsc: bool,) -> list:
    list_document__daily_revenue =self.getDailyRevenue()
    sorted_document_revenues = sorted(list_document__daily_revenue, key=lambda x: x["DoanhThu"], reverse=isAsc)
    return sorted_document_revenues
  
  #do đề bài ko có tên nên xếp theo mã 
  def sortDocumentByDocumentName(self, isAsc: bool) -> list:
    by_id_document = self.list
    if (isAsc == True):
        for i in range(len(by_id_document) - 1):
            for j in range(i + 1, len(by_id_document)):
                if (by_id_document[i].getmaTaiLieu() > by_id_document[j].getmaTaiLieu()):
                    temp = by_id_document[i]
                    by_id_document[i] = by_id_document[j]
                    by_id_document[j] = temp
    else:
        for i in range(len(by_id_document) - 1):
            for j in range(i + 1, len(by_id_document)):
                if (by_id_document[i].getmaTaiLieu() < by_id_document[j].getmaTaiLieu()):
                    temp = by_id_document[i]
                    by_id_document[i] = by_id_document[j]
                    by_id_document[j] = temp
    print("Danh sách sau khi sắp xếp:")
    return by_id_document
    
  def getTopDocuments(self) -> list:
    sorted_amount_invent = sorted(Self.list, key=lambda x: x.getsoLuongTonKho(),reverse=True)
    top_five_amount_invent = sorted_amount_invent[:5]
    sorted_document_revenues = self.sortDocumentByDailyRevenuef(1)
    top_five_document_revenues = sorted_document_revenues[:5]
    list_document_best_seller = {
      "Top5TonKhoMin": top_five_amount_invent,
      "Top5BanChay": top_five_document_revenues
    }
    return list_document_best_seller

  def editDocument(self,doc: Document,new_info) -> Document:
    for document in self.list:
        if document.getmaTaiLieu() == doc.getmaTaiLieu():
            if 'nha_xuat_ban' in new_info:
                document.setnhaXuatBan(new_info['nha_xuat_ban'])
            if 'so_luong_ton_kho' in new_info:
                document.setsoLuongTonKho(new_info['so_luong_ton_kho'])
            if 'gia_ban' in new_info:
                document.setgiaBan(new_info['gia_ban'])

            if isinstance(document, Sach):
                if 'ten_tac_gia' in new_info:
                    document.settenTacGia(new_info['ten_tac_gia'])
                if 'so_trang' in new_info:
                    document.setsoTrang(new_info['so_trang'])
            elif isinstance(document, TapChi):
                if 'so_phat_hanh' in new_info:
                    document.setsoPhatHanh(new_info['so_phat_hanh'])
                if 'thang_phat_hanh' in new_info:
                    document.setthangPhatHanh(new_info['thang_phat_hanh'])
            elif isinstance(document, Bao):
                if 'ngay_phat_hanh' in new_info:
                    document.setngayThangNamPhatHanh(new_info['ngay_phat_hanh'])
        return document  

  def deleteDocument(self,doc: Document) -> None:
    for i in range(len(self.list)):
            if self.list[i].getmaTaiLieu() == doc.getmaTaiLieu():
                del self.list[i]
                print(f"Đã xóa tài liệu có mã {doc.getmaTaiLieu()}")
                break
            else:
                print(f"Không tìm thấy tài liệu có mã {doc.getmaTaiLieu()} để xóa.")


if __name__ == "__main__":
    print("Chọn 1 trong các chức năng sau:")
    print("1. Thêm mới tài liệu")
    print("2. Tìm kiếm tài liệu")
    print("3. Sửa thông tin tài liệu")
    print("4. Sắp xếp tài liệu dựa vào doanh thu theo ngày")
    print("5. Sắp xếp tài liệu theo tên")
    print("6. Hiển thị danh sách tạp chí, báo có cùng tháng phát hàng")
    print("7. Hiển thị tài liệu bán chạy cần nhập thêm")
    print("8. Sửa thông tin tài liệu")
    print("9. Xoá tài liệu")
    print("10. Thoát chương trình")


    choice = int(input("Lựa chọn: "))
    if (choice == 1):
        pass
    if (choice == 2):
        pass
    if (choice == 3):
        pass
    if (choice == 4):
        pass
    if (choice == 5):
        pass
    if (choice == 6):
        pass
    if (choice == 7):
        pass
    if (choice == 8):
        pass
    if (choice == 9):
        pass
    if (choice == 10):
        pass
